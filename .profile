#!/bin/sh
# Profile file. Runs on login.

# Adds `~/.local/bin` and all subdirectories to $PATH
export PATH="$PATH:$(du "$HOME/.local/bin/" | cut -f2 | tr '\n' ':' | sed 's/:*$//'):$HOME/.local/go/bin"
#export PATH="/usr/lib/ccache/bin${PATH:+:}$PATH"
export EDITOR="nvim"
export TERMINAL="alacritty"
#export BROWSER="open-firefox"
export READER="zathura"
export FILE="lf"
export BIB="$HOME/Documents/LaTeX/uni.bib"
export REFER="$HOME/Documents/referbib"
export SUDO_ASKPASS="$HOME/.local/bin/tools/dmenupass"
export NOTMUCH_CONFIG="$HOME/.config/notmuch-config"
export GTK2_RC_FILES="$HOME/.config/gtk-2.0/gtkrc-2.0"
export QT_QPA_PLATFORMTHEME="qt5ct"
export GOPATH="$HOME/.local/go"
# export CCACHE_DIR="~/.ccache"
#Needed so that makepkg doesn't complain that PACKAGER doesn't have a proper format
unset PACKAGER
# less/man colors
export LESS=-R
export LESS_TERMCAP_mb="$(printf '%b' '[1;31m')"; a="${a%_}"
export LESS_TERMCAP_md="$(printf '%b' '[1;36m')"; a="${a%_}"
export LESS_TERMCAP_me="$(printf '%b' '[0m')"; a="${a%_}"
export LESS_TERMCAP_so="$(printf '%b' '[01;44;33m')"; a="${a%_}"
export LESS_TERMCAP_se="$(printf '%b' '[0m')"; a="${a%_}"
export LESS_TERMCAP_us="$(printf '%b' '[1;32m')"; a="${a%_}"
export LESS_TERMCAP_ue="$(printf '%b' '[0m')"; a="${a%_}"
#Wayland stuff
export QT_QPA_PLATFORM="wayland;xcb"
export SDL_VIDEODRIVER="wayland"
export _JAVA_AWT_WM_NONREPARENTING=1
export PICO_SDK_PATH=~/Sources/pico-sdk
#Use positional layout
#export SDL_GAMECONTROLLER_USE_BUTTON_LABELS=0

[ ! -f ~/.config/shortcutrc ] && shortcuts >/dev/null 2>&1
[ ! -f ~/.config/aliasrc ] && source ~/.config/aliasrc
[ ! -f ~/.config/wechall ] && source ~/.config/wechall #Private wechall stuff
#Add a function for reading markdown files(inspired by https://tosbourn.com/view-markdown-files-terminal/)
wmd () {
	pandoc $1 | lynx -stdin
}
# Start graphical server if i3 not already running.
 [ "$(tty)" = "/dev/tty1" ] && exec dbus-launch --exit-with-session sway

# Switch escape and caps if tty:
sudo -n loadkeys ~/.local/bin/ttymaps.kmap 2>/dev/null
