#!/bin/sh

URL="https://old.reddit.com/message/unread/.json?feed=79e41b76a40f103e6be6f5a45bff1335b0378d8f&user=Ultracoolguy4"
USERAGENT="polybar-scripts/notification-reddit:v1.0 u/Ultracoolguy4"

notifications=$(curl -sf --user-agent "$USERAGENT" "$URL" | jq '.["data"]["children"] | length')

if [ -n "$notifications" ] && [ "$notifications" -gt 0 ]; then
    echo "Reddit $notifications"
else
    echo ""
fi
